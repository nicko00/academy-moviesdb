package com.example.moviesdb.fragments

import android.os.Bundle
import android.text.TextUtils
import android.text.method.ScrollingMovementMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviesdb.R
import com.example.moviesdb.databinding.FragmentDetalhesBinding
import com.squareup.picasso.Picasso
import database.FavoritosViewModel
import database.entity.FilmeFav

class DetalhesFragment : Fragment() {

    private lateinit var binding : FragmentDetalhesBinding
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var args: DetalhesFragmentArgs
    private lateinit var capaFilme: ImageView
    private lateinit var mFavoritosViewModel: FavoritosViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detalhes, container, false)
        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        args = DetalhesFragmentArgs.fromBundle(requireArguments())

        binding.tituloDetalhesFilme.text = args.filme.title
        binding.dataDetalheFilme.text = args.filme.releaseDate
        binding.sinopseDetalheFilme.text = args.filme.sinopse
        capaFilme = binding.capaFilmeDetalhes
        Picasso.get().load("https://image.tmdb.org/t/p/w500/" + args.filme.posterPath).into(capaFilme)

        // favoritos
        mFavoritosViewModel = ViewModelProvider(this).get(FavoritosViewModel::class.java)

        binding.buttonFav.setOnClickListener {
            insertDataToDatabse()
        }

        return binding.root
    }

    private fun insertDataToDatabse() {
        val titulo = binding.tituloDetalhesFilme.text.toString()
        val capa = args.filme.posterPath
        if (inputCheck(titulo)) {
            val filmeFav = FilmeFav(0, capa, titulo)
            mFavoritosViewModel.insert(filmeFav)
            Toast.makeText(requireContext(), "Filme adicionado aos seus favoritos", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(requireContext(), "Erro ao adicionar filme aos seus favoritos", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(titulo: String): Boolean {
        return !(TextUtils.isEmpty(titulo))
    }

}