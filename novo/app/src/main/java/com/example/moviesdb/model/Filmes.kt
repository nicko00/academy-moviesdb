package com.example.moviesdb.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Filmes(
    @SerializedName("title")
    val title: String?,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("vote_average")
    val rating: String?,
    @SerializedName("release_date")
    val releaseDate: String?,
    @SerializedName("overview")
    val sinopse : String?,
    @SerializedName("id")
    val id : Int?

) : Serializable
