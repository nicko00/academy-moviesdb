package com.example.moviesdb.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface FilmesService {
    @GET("movie/upcoming?api_key=$apiKey&language=$idioma")
    fun getFilmesPorVir(
        @Query("page") pagina : Int
    ): Call<FilmesResult>

    @GET("movie/popular?api_key=$apiKey&language=$idioma")
    fun getFilmesPopulares(
        @Query("page") pagina : Int
    ) : Call<FilmesResult>

    @GET("movie/top_rated?api_key=$apiKey&language=$idioma")
    fun getFilmesAvaliados(
        @Query("page") pagina : Int
    ) : Call<FilmesResult>

    @GET("search/movie?api_key=$apiKey&language=$idioma")
    fun getPesquisarFilmes(
        //@Query("page") pagina: Int,
        @Query("query") busca: String
    ): Call<FilmesResult>


    companion object {
        private const val apiKey = "49a125704ddd68243c8f67781ed47620"
        private const val idioma = "pt-BR"
    }
}