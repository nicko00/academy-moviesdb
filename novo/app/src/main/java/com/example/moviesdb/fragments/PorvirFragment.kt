package com.example.moviesdb.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesdb.R
import com.example.moviesdb.adapter.PorVirAdapter
import com.example.moviesdb.databinding.FragmentPorvirBinding
import com.example.moviesdb.listener.FilmeOnItemClickListener
import com.example.moviesdb.model.Filmes
import com.example.moviesdb.network.ApiService
import com.example.moviesdb.network.FilmesResult
import com.google.android.material.progressindicator.CircularProgressIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PorvirFragment : Fragment(), FilmeOnItemClickListener {
    // TODO: Rename and change types of parameters
    private lateinit var listFilmes : List<Filmes>
    private lateinit var binding : FragmentPorvirBinding
    private lateinit var layoutManager : LinearLayoutManager
    private var porVirAdapter = PorVirAdapter(this)
    private var pagina = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_porvir, container, false)
        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerviewFilmesPorVir.adapter = porVirAdapter
        binding.recyclerviewFilmesPorVir.layoutManager = layoutManager
        getFilmesPorVir()
        return binding.root
    }
    private fun getFilmesPorVir() {
        val call = ApiService().filmesService().getFilmesPorVir(pagina)
        call.enqueue(object : Callback<FilmesResult> {
            override fun onResponse(call: Call<FilmesResult>, response: Response<FilmesResult>) {
                val retornoapi = response.body()
                if(retornoapi != null) {
                    listFilmes = retornoapi.filmesResult
                    porVirAdapter.submitList(listFilmes)
                    getPagina()
                }
            }

            override fun onFailure(call: Call<FilmesResult>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }

    fun getPagina() {
        binding.recyclerviewFilmesPorVir.addOnScrollListener(object :RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val visibleItemCount = layoutManager.childCount
                val pastVisibleItem = layoutManager.findFirstCompletelyVisibleItemPosition()
                val total = porVirAdapter.itemCount
                if ((visibleItemCount + pastVisibleItem) >= total){
                    binding.recyclerviewFilmesPorVir.removeOnScrollListener(this)
                    pagina++
                    getFilmesPorVir()
                }

                super.onScrolled(recyclerView, dx, dy)

            }

        })

    }
    override fun onItemClickFilme(filmes: Filmes) {
        findNavController().navigate(
            PorvirFragmentDirections.actionPorvirFragmentToDetalhesFragment(filmes)
        )
    }
}