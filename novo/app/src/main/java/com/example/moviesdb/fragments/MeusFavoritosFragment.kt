package com.example.moviesdb.fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviesdb.R
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesdb.adapter.FavoritosAdapter
import com.example.moviesdb.databinding.FragmentMeusFavoritosBinding
import com.example.moviesdb.model.Filmes
import database.FavoritosViewModel
import database.entity.FilmeFav

class MeusFavoritosFragment : Fragment() {
    private lateinit var listFilmes : List<FilmeFav>
    private val favoritosAdapter = FavoritosAdapter()
    private lateinit var binding : FragmentMeusFavoritosBinding
    private lateinit var layoutManager: LinearLayoutManager
    private var pagina = 1
    private lateinit var mFavoritosViewModel: FavoritosViewModel
    private val args by navArgs<MeusFavoritosFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_meus_favoritos, container, false)
        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        binding.recyclerviewFilmesFavoritos.adapter = favoritosAdapter
        binding.recyclerviewFilmesFavoritos.layoutManager = layoutManager

        mFavoritosViewModel = ViewModelProvider(this).get(FavoritosViewModel::class.java)
        mFavoritosViewModel.findAll.observe(viewLifecycleOwner, Observer { filmeFav ->
            favoritosAdapter.setData(filmeFav)
        })

        setHasOptionsMenu(true)
//        setRecyclerSwipe()
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_delete){
            deleteMovie()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteMovie() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Excluir?")
        builder.setMessage("Tem certeza que deseja excluir ?")
        builder.setPositiveButton("Sim",
        DialogInterface.OnClickListener {
                dialogInterface, id -> mFavoritosViewModel.deleteAll()
                var filmesList = mutableListOf(emptyList<FilmeFav>())
        })
        builder.setNegativeButton("Não"){ _,_ ->

        }

        builder.create().show()
    }

//    private fun setRecyclerSwipe(){
//        val swipeHandler = object : ItemTouchHelper.SimpleCallback
//            (0, ItemTouchHelper.START or ItemTouchHelper.END){
//            override fun onMove(
//                recyclerView: RecyclerView,
//                viewHolder: RecyclerView.ViewHolder,
//                target: RecyclerView.ViewHolder
//            ): Boolean {
//                return false
//            }
//
//            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
//                val movieId = favoritosAdapter.getMovieByPosition(viewHolder.bindingAdapterPosition)
//                mFavoritosViewModel.deleteFav(movieId, favoritosAdapter.itemCount)
//                favoritosAdapter.removeAt(viewHolder.bindingAdapterPosition)
//            }
//
//        }
//        val itemTouchHelper = ItemTouchHelper(swipeHandler)
//        itemTouchHelper.attachToRecyclerView(binding.recyclerviewFilmesFavoritos)
//    }
}