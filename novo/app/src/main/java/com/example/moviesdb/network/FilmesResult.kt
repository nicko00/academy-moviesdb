package com.example.moviesdb.network

import com.example.moviesdb.model.Filmes
import com.google.gson.annotations.SerializedName

data class FilmesResult(
    @SerializedName("results")
    val filmesResult: List<Filmes>
)
