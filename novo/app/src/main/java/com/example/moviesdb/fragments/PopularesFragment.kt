package com.example.moviesdb.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesdb.MainActivity
import com.example.moviesdb.R
import com.example.moviesdb.adapter.PopularesAdapter
import com.example.moviesdb.databinding.FragmentPopularesBinding
import com.example.moviesdb.listener.FilmeOnItemClickListener
import com.example.moviesdb.model.Filmes
import com.example.moviesdb.network.ApiService
import com.example.moviesdb.network.FilmesResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PopularesFragment : Fragment(), FilmeOnItemClickListener {
    // TODO: Rename and change types of parameters
    private lateinit var listFilmes : List<Filmes>
    private val popularesAdapter = PopularesAdapter(this)
    private lateinit var binding : FragmentPopularesBinding
    private lateinit var layoutManager: LinearLayoutManager
    private var pagina = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_populares, container, false)
        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerviewFilmesPopulares.adapter = popularesAdapter
        binding.recyclerviewFilmesPopulares.layoutManager = layoutManager
        getFilmesPopulares()
        return binding.root
    }
    private fun getFilmesPopulares() {
        val call = ApiService().filmesService().getFilmesPopulares(pagina)
        call.enqueue(object : Callback<FilmesResult> {
            override fun onResponse(call: Call<FilmesResult>, response: Response<FilmesResult>) {
                val retornoApi = response.body()
                if(retornoApi != null){
                    listFilmes = retornoApi.filmesResult
                    popularesAdapter.submitList(listFilmes)
                    getPagina()
                }

            }

            override fun onFailure(call: Call<FilmesResult>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun getPagina() {
        binding.recyclerviewFilmesPopulares.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val visibleItemCount = layoutManager.childCount
                val pastVisibleItem = layoutManager.findFirstCompletelyVisibleItemPosition()
                val total = popularesAdapter.itemCount
                if ((visibleItemCount + pastVisibleItem) >= total){
                    binding.recyclerviewFilmesPopulares.removeOnScrollListener(this)
                    pagina++
                    getFilmesPopulares()
                }

                super.onScrolled(recyclerView, dx, dy)

            }

        })

    }

    override fun onItemClickFilme(filmes: Filmes) {
        findNavController().navigate(
            PopularesFragmentDirections.actionPopularesFragmentToDetalhesFragment(filmes)
        )
    }

}