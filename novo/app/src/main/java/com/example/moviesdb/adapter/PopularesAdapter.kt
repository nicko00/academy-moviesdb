package com.example.moviesdb.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesdb.R
import com.example.moviesdb.listener.FilmeOnItemClickListener
import com.example.moviesdb.model.Filmes
import com.squareup.picasso.Picasso

class PopularesAdapter (private var clickListener: FilmeOnItemClickListener):
    RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private var filmes : MutableList<Filmes> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FilmesPopularesViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.card_filmes_populares, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is FilmesPopularesViewHolder -> {
                holder.bind(filmes[position], clickListener)
            }
        }
    }

    override fun getItemCount(): Int {
        return filmes.size
    }

    fun submitList (filmesList : List<Filmes>) {
        filmes.addAll(filmesList)
        notifyDataSetChanged()
    }

    class FilmesPopularesViewHolder constructor(itemView : View) : RecyclerView.ViewHolder(itemView){
        val titulo : TextView = itemView.findViewById(R.id.tituloFilmePopular)
        val capa : ImageView = itemView.findViewById(R.id.capaFilmePopular)

        fun bind(filme : Filmes, action: FilmeOnItemClickListener){
            titulo.text = filme.title
            Picasso.get().load("https://image.tmdb.org/t/p/w500/${filme.posterPath}").into(capa)

            itemView.setOnClickListener() {
                action.onItemClickFilme(filme)
            }
        }
    }
}