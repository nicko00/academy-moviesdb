package com.example.moviesdb.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FilmesDetalhes {
    @GET("movie")
    fun getFilmesDetalhes(
        @Query("movie_id") movieId : String,
        @Query("api_key") apiKey : String = "49a125704ddd68243c8f67781ed47620",
        @Query("language") idioma : String = "pt-BR"
    ) : Call<FilmesResult>
}