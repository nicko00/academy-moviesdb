package com.example.moviesdb.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesdb.R
import com.example.moviesdb.listener.FilmeOnItemClickListener
import com.example.moviesdb.model.Filmes
import com.squareup.picasso.Picasso



    class AvaliadosAdapter(private var clickListener: FilmeOnItemClickListener) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private var filmes: MutableList<Filmes> = ArrayList()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return FilmesAvaliadosViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.card_filmes_avaliados, parent, false)
            )
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            when (holder) {
                is FilmesAvaliadosViewHolder -> {
                    holder.bind(filmes[position], clickListener)
                }
            }
        }

        fun submitList(filmesList: List<Filmes>) {
            filmes.addAll(filmesList)
            notifyDataSetChanged()
        }

        override fun getItemCount(): Int {
            return filmes.size
        }

        class FilmesAvaliadosViewHolder constructor(itemView : View) : RecyclerView.ViewHolder(itemView){
            val titulo: TextView = itemView.findViewById(R.id.tituloFilmeAvaliado)
            val capa: ImageView = itemView.findViewById(R.id.capaFilmeAvaliado)
            val avaliacao: TextView = itemView.findViewById(R.id.avaliacao)

            fun bind(filme: Filmes,  action: FilmeOnItemClickListener) {
                titulo.text = filme.title
                Picasso.get().load("https://image.tmdb.org/t/p/w500/${filme.posterPath}").into(capa)
                avaliacao.text = filme.rating

                itemView.setOnClickListener() {
                    action.onItemClickFilme(filme)
                }
            }




        }

    }
