package com.example.moviesdb.listener

import com.example.moviesdb.model.Filmes

interface FilmeOnItemClickListener {
    fun onItemClickFilme(filmes: Filmes)
}