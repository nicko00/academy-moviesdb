package com.example.moviesdb.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class ApiService {
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.themoviedb.org/3/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun filmesService() : FilmesService = this.retrofit.create(FilmesService::class.java)
    fun filmesServiceDetalhes() : FilmesDetalhes = this.retrofit.create(FilmesDetalhes::class.java)
}