package com.example.moviesdb.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesdb.R
import com.example.moviesdb.model.Filmes
import com.squareup.picasso.Picasso
import database.entity.FilmeFav

class FavoritosAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var filmeList = emptyList<FilmeFav>()
    private var filmes: MutableList<Filmes> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FavoritosViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.card_filmes_favoritos, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is FavoritosViewHolder -> {
                holder.bind(filmeList[position])
            }
        }
    }

     fun submitList(listFilmes: MutableList<Filmes>) {
        filmes.addAll(listFilmes)
        notifyDataSetChanged()
    }

    fun setData(filmeFav: List<FilmeFav>) {
        this.filmeList = filmeFav.toMutableList()
        notifyDataSetChanged()
    }

    fun getMovieByPosition(position: Int): Int {
        return filmeList[position].id!!
    }


    fun removeAt(position: Int) {
        filmes.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun getItemCount(): Int {
        return filmeList.size
    }

    class FavoritosViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titulo : TextView = itemView.findViewById(R.id.tituloFilmeFavorito)
        val capa : ImageView = itemView.findViewById(R.id.capaFilmeFavorito)

        fun bind(filmeFav: FilmeFav) {
            titulo.text = filmeFav.titulo
             Picasso.get().load("https://image.tmdb.org/t/p/w500/${filmeFav.posterPath}").into(capa)
        }
    }

}