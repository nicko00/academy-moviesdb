package com.example.moviesdb.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviesdb.R
import com.example.moviesdb.adapter.PesquisarAdapter
import com.example.moviesdb.databinding.FragmentPesquisarBinding
import com.example.moviesdb.listener.FilmeOnItemClickListener
import com.example.moviesdb.model.Filmes
import com.example.moviesdb.network.ApiService
import com.example.moviesdb.network.FilmesResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PesquisarFragment : Fragment() ,FilmeOnItemClickListener {
    private var pesquisarAdapter = PesquisarAdapter(this)
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var binding : FragmentPesquisarBinding
    private lateinit var listFilmes : List<Filmes>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pesquisar, container, false)
        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerviewPesquisar.adapter = pesquisarAdapter
        binding.recyclerviewPesquisar.layoutManager = layoutManager
        val searchView = binding.busca

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(text: String?): Boolean {
               return false
            }

            override fun onQueryTextChange(text: String): Boolean {
                getPesquisarFilmes(text)
                return false
            }

        })
        return binding.root
    }
    private fun getPesquisarFilmes(texto : String) {
        val call = ApiService().filmesService().getPesquisarFilmes(texto)
        call.enqueue(object : Callback<FilmesResult> {
            override fun onResponse(call: Call<FilmesResult>, response: Response<FilmesResult>) {
                val retornoApi = response.body()
                if(retornoApi != null){
                    listFilmes = retornoApi.filmesResult
                    pesquisarAdapter.submitList(listFilmes)
                }

            }

            override fun onFailure(call: Call<FilmesResult>, t: Throwable) {
                Log.i(texto,"falhou")
            }
        })
    }
    override fun onItemClickFilme(filmes: Filmes) {
        findNavController().navigate(
            PesquisarFragmentDirections.actionPesquisarFragmentToDetalhesFragment(filmes)
        )
    }

}