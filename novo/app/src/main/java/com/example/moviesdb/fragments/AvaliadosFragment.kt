package com.example.moviesdb.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesdb.R
import com.example.moviesdb.adapter.AvaliadosAdapter
import com.example.moviesdb.adapter.PopularesAdapter
import com.example.moviesdb.databinding.FragmentAvaliadosBinding
import com.example.moviesdb.listener.FilmeOnItemClickListener
import com.example.moviesdb.model.Filmes
import com.example.moviesdb.network.ApiService
import com.example.moviesdb.network.FilmesResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AvaliadosFragment : Fragment(), FilmeOnItemClickListener {
    // TODO: Rename and change types of parameters
    private lateinit var listFilmes : List<Filmes>
    private lateinit var binding : FragmentAvaliadosBinding
    private lateinit var layoutManager: LinearLayoutManager
    private var pagina = 1
    private var avaliadosAdapter = AvaliadosAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_avaliados, container, false)
        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerviewFilmesAvaliados.adapter = avaliadosAdapter
        binding.recyclerviewFilmesAvaliados.layoutManager = layoutManager
        getFilmesAvaliados()
        return binding.root
    }
    private fun getFilmesAvaliados() {
        val call = ApiService().filmesService().getFilmesAvaliados(pagina)
        call.enqueue(object : Callback<FilmesResult>{
            override fun onResponse(call: Call<FilmesResult>, response: Response<FilmesResult>) {
                val respostaApi = response.body()
                if(respostaApi != null) {
                     listFilmes = respostaApi.filmesResult
                     avaliadosAdapter.submitList(listFilmes)
                    getPagina()
                }

            }

            override fun onFailure(call: Call<FilmesResult>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }

    fun getPagina() {
        binding.recyclerviewFilmesAvaliados.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val visibleItemCount = layoutManager.childCount
                val pastVisibleItem = layoutManager.findFirstCompletelyVisibleItemPosition()
                val total = avaliadosAdapter.itemCount
                if ((visibleItemCount + pastVisibleItem) >= total){
                    binding.recyclerviewFilmesAvaliados.removeOnScrollListener(this)
                    pagina++
                    getFilmesAvaliados()
                }



            }

        })

    }

    override fun onItemClickFilme(filmes: Filmes) {
        findNavController().navigate(
            AvaliadosFragmentDirections.actionAvaliadosFragmentToDetalhesFragment(filmes)
        )
    }

}