package database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.moviesdb.model.Filmes
import database.entity.FilmeFav


@Dao
interface FavDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(filmeFav : FilmeFav)

    @Query("DELETE FROM tb_filme WHERE id IN (:filmeFav)")
    suspend fun deleteFav(filmeFav: Int)

    @Query("DELETE FROM tb_filme")
    suspend fun deleteAll()

    @Query("SELECT * FROM tb_filme ORDER BY id ASC")
    fun findAll() : LiveData<List<FilmeFav>>
}