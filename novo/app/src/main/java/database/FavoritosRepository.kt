package database

import androidx.lifecycle.LiveData
import com.example.moviesdb.model.Filmes
import database.entity.FilmeFav

class FavoritosRepository (private val favDao: FavDao){
    val findAll : LiveData<List<FilmeFav>> = favDao.findAll()

    suspend fun insert(filmeFav: FilmeFav){
        favDao.insert(filmeFav)
    }

    suspend fun deleteFav(filmeFav: Int){
        favDao.deleteFav(filmeFav)
    }

    suspend fun deleteAll(){
        favDao.deleteAll()
    }

}