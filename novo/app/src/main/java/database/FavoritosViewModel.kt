package database

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.moviesdb.model.Filmes
import database.entity.FilmeFav
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import database.FavoritosRepository
import database.FavDao
import kotlinx.coroutines.InternalCoroutinesApi
import java.text.FieldPosition


@OptIn(InternalCoroutinesApi::class)
class FavoritosViewModel (application : Application) : AndroidViewModel(application){
    val findAll : LiveData<List<FilmeFav>>
    private var repository : FavoritosRepository

    init {
        val favoritoDao = favoritosDB.getDatabase(application).favDao()
        repository = FavoritosRepository(favoritoDao)
        findAll = repository.findAll
    }

    fun insert(fav: FilmeFav) {
        viewModelScope.launch(Dispatchers.IO){
            repository.insert(fav)
        }
    }

    fun deleteFav(fav: Int, position : Int){
        viewModelScope.launch(Dispatchers.IO){
            repository.deleteFav(fav)
        }
    }

    fun deleteAll(){
        viewModelScope.launch(Dispatchers.IO){
            repository.deleteAll()
        }
    }
}