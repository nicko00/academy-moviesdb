package database.entity

import android.widget.ImageView
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "tb_filme")
data class FilmeFav (
    @PrimaryKey(autoGenerate = true)
    val id : Int,
    val posterPath: String?,
    val titulo : String
    ){
}