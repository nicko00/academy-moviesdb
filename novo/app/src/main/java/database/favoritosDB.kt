package database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import database.entity.FilmeFav
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.internal.synchronized
import java.security.AccessControlContext

@Database(entities = [FilmeFav::class], version = 1, exportSchema = false)
abstract class favoritosDB :RoomDatabase(){
    abstract fun favDao() : FavDao

    companion object {

        @Volatile
        private var INSTANCE : favoritosDB? = null

        @InternalCoroutinesApi
        fun getDatabase(context: Context) : favoritosDB {
            val tempInstance = INSTANCE

            if (tempInstance != null){
                return tempInstance
            }

            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    favoritosDB::class.java,
                    "favoritos_database"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }
}